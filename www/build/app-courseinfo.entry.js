import { r as registerInstance, h } from './core-9a4b53af.js';

const AppCourseInfo = class {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    componentWillRender() {
        fetch(`http://localhost:3000/api/v1/courses/${this.id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
        })
            .then(response => response.json())
            .then(json => this.courseinfo = json);
        console.log(this.courseinfo);
    }
    courseinfocard() {
        return [
            h("app-coursecard", { course_name: this.courseinfo.course_name, course_number: this.courseinfo.course_number, section_number: this.courseinfo.section_number, term: this.courseinfo.term, instructor: this.courseinfo.instructor, id: this.courseinfo.id })
        ];
    }
    render() {
        return [
            h("ion-header", null, h("ion-toolbar", { color: "purple" }, h("ion-buttons", { slot: "start" }, h("ion-back-button", { defaultHref: "/courses" })), h("ion-title", null, "COURSE INFORMATION"))),
            h("ion-content", { class: "ion-padding" }, this.courseinfocard())
        ];
    }
    static get style() { return ""; }
};

export { AppCourseInfo as app_courseinfo };
