import { r as registerInstance, h } from './core-9a4b53af.js';

const AppHome = class {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return [
            h("ion-content", { color: "purple", class: "ion-padding" }, h("ion-grid", null, h("ion-row", { "align-items-center": true, style: { height: "100vh" }, "justify-content-around": true }, h("ion-col", { "align-self-center": true }, h("ion-item", { color: "purple" }, h("img", { src: "../assets/icon/sillibi-logo.png", style: { marginTop: "120px" } })), h("ion-button", { class: "Fbutton", href: "https://facebook.com", expand: "full", style: { marginTop: "120px" }, color: "blue" }, " LOGIN USING FACEBOOK "), h("ion-button", { class: "Ebutton", href: "https://gmail.com", expand: "full", style: { marginTop: "10px", marginBottom: "80px" }, color: "yellow" }, " LOGIN USING EMAIL "), h("p", null, " ", h("div", { "text-center": true }, " Don't want to use facebook? "), " "), h("ion-button", { class: "CreateAccount", href: "/profile", fill: "clear" }, "CREATE AN ACCOUNT")))))
        ];
    }
    static get style() { return ".Fbutton {\n  height: 40px;\n  width: 70%;\n  margin:0 auto;\n}\n\n.Ebutton {\n  height: 40px;\n  width: 70%;\n  margin:0 auto;\n}\n\n.CreateAccount {\n  margin:0 auto;\n  width: 100%\n}"; }
};

export { AppHome as app_home };
