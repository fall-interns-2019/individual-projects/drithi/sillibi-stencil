import { r as registerInstance, h } from './core-9a4b53af.js';

const AppCourses = class {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return [
            h("ion-header", null, h("ion-toolbar", { color: "purple" }, h("ion-buttons", { slot: "start" }, h("ion-back-button", { defaultHref: "/profile" })), h("ion-title", null, "MY COURSES"))),
            h("ion-content", { class: "ion-padding" }, h("a", { href: "/addcourse" }, " ", h("img", { src: "../assets/icon/illo_AddCourse.svg", class: "center" }), " "), h("p", null, " ", h("div", { "text-center": true }, " Add a course to upload a syllabus "), " "))
        ];
    }
    static get style() { return ".center {\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n  margin-top: 60px;\n  width: 80%;\n}"; }
};

export { AppCourses as app_courses };
