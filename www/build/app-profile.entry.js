import { r as registerInstance, h } from './core-9a4b53af.js';

const AppProfile = class {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return [
            h("ion-header", null, h("ion-toolbar", { color: "purple" }, h("ion-buttons", { slot: "start" }, h("ion-back-button", { defaultHref: "/" })), h("ion-title", null, "Create Account"))),
            h("ion-content", { class: "ion-padding" }, h("ion-item", null, h("ion-label", { position: "stacked" }, " First Name ", h("ion-text", { color: "danger" }, " * "), " "), h("ion-input", { required: true, type: "text" }, " ")), h("ion-item", null, h("ion-label", { position: "stacked" }, " Last Name ", h("ion-text", { color: "danger" }, " * "), " "), h("ion-input", { required: true, type: "text" }, " ")), h("ion-item", null, h("ion-label", { position: "stacked" }, " Email Address ", h("ion-text", { color: "danger" }, " * "), " "), h("ion-input", { required: true, type: "text" }, " ")), h("ion-item", null, h("ion-label", { position: "stacked" }, " Password ", h("ion-text", { color: "danger" }, " * "), " "), h("ion-input", { required: true, type: "password" }, " ")), h("ion-button", { expand: "full", style: { marginTop: "30px", marginBottom: "40px" }, color: "yellow" }, " CREATE ACCOUNT "), h("p", null, " ", h("div", { "text-center": true }, " Already have an account? "), " "), h("ion-button", { class: "signIn", href: "/sign", fill: "clear" }, "SIGN IN"))
        ];
    }
    static get style() { return ".signIn {\n  width: 100%;\n  margin: 0 auto 0;\n}"; }
};

export { AppProfile as app_profile };
