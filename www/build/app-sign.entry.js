import { r as registerInstance, h } from './core-9a4b53af.js';

const AppSign = class {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return [
            h("ion-header", null, h("ion-toolbar", { color: "purple" }, h("ion-buttons", { slot: "start" }, h("ion-back-button", { defaultHref: "/profile" })), h("ion-title", null, "Sign In"))),
            h("ion-content", { class: "ion-padding" }, h("ion-item", null, h("ion-input", { required: true, type: "text", placeholder: "Email Address" })), h("ion-item", null, h("ion-input", { required: true, type: "password", placeholder: "Password" })), h("ion-button", { href: "/courses", expand: "full", style: { marginTop: "30px", marginBottom: "20px" }, color: "yellow" }, " LOGIN "), h("ion-button", { href: "https://facebook.com", expand: "full", style: { marginBottom: "40px" }, color: "blue" }, " LOGIN USING FACEBOOK "), h("p", null, " ", h("div", { "text-center": true }, " Don't have an account? "), " "), h("ion-button", { class: "Register", href: "/profile", fill: "clear" }, "REGISTER"))
        ];
    }
    static get style() { return ".ion-padding {\n  margin-top: 100px;\n}\n\n.Register {\n  margin:0 auto;\n  width: 100%\n}"; }
};

export { AppSign as app_sign };
