import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {

  render() {
    return (
      <ion-app>
        <ion-router useHash={false}>
          <ion-route url="/" component="app-home" />
          <ion-route url="/profile/" component="app-profile" />
          <ion-route url="/sign/" component="app-sign" />
          <ion-route url="/courses/" component="app-courses" />
          <ion-route url="/addcourse/" component="app-addcourse" />
          <ion-route url="/coursecard" component="app-coursecard" />
          <ion-route url= "/courselist" component="app-courselist" />
          <ion-route url="/courseinfo/:id" component="app-courseinfo" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
