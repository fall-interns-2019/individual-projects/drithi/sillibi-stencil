import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-profile',
  styleUrl: 'app-profile.css'
})
export class AppProfile {

  render() {
    return [
      <ion-header>
        <ion-toolbar color="purple">
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/" />
          </ion-buttons>
          <ion-title>Create Account</ion-title>
        </ion-toolbar>
      </ion-header>,

      <ion-content class="ion-padding">

        <ion-item>
          <ion-label position={"stacked"}> First Name <ion-text color ="danger"> * </ion-text> </ion-label>
          <ion-input required type={"text"}> </ion-input>
        </ion-item>

        <ion-item>
          <ion-label position={"stacked"}> Last Name <ion-text color ="danger"> * </ion-text> </ion-label>
          <ion-input required type={"text"}> </ion-input>
        </ion-item>

        <ion-item>
          <ion-label position={"stacked"}> Email Address <ion-text color ="danger"> * </ion-text> </ion-label>
          <ion-input required type={"text"}> </ion-input>
        </ion-item>

        <ion-item>
          <ion-label position={"stacked"}> Password <ion-text color ="danger"> * </ion-text> </ion-label>
          <ion-input required type={"password"}> </ion-input>
        </ion-item>

        <ion-button expand={"full"} style={{marginTop: "30px", marginBottom: "40px"}} color={"yellow"}> CREATE ACCOUNT </ion-button>
        <p> <div text-center> Already have an account? </div> </p>
        <ion-button class="signIn" href={"/sign"} fill={"clear"}>SIGN IN</ion-button>

      </ion-content>

    ];
  }
}


