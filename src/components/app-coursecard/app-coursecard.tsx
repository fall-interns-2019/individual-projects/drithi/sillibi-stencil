import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'app-coursecard',
  styleUrl: 'app-coursecard.css'
})
export class AppCoursecard {
  courses: any[];
  @Prop() course_name:string;
  @Prop() course_number:string;
  @Prop() section_number:string;
  @Prop() term:string;
  @Prop() instructor:string;
  @Prop() id:string;

  async componentWillLoad(){
    this.getCourseinfo()
  }

  async getCourseinfo(){
    const response = await fetch("http://localhost:3000/api/v1/courses");
    const coursesinfo = await response.json();
    this.courses = coursesinfo
  }

  render() {
    return [
      <ion-card href={`/courseinfo/${this.id}`}>
          <ion-list lines={"full"}>
        <ion-item>
           <ion-text> {this.course_name} </ion-text>
        </ion-item>
            <ion-item lines={'full'}>
              <ion-label>
                <ion-note>
                  {this.course_number} - {this.section_number} <br/> {this.instructor}
                </ion-note>
              </ion-label>
            </ion-item>
        <ion-item>
          <ion-icon slot={"start"} name="document"/>
          <ion-text> Syllabus </ion-text>
          <ion-icon slot={"end"} name="add-circle-outline"> </ion-icon>
        </ion-item>

        <ion-item>
          <ion-icon slot={"start"} name="paper"> </ion-icon> <ion-text> Assignments </ion-text>
          <ion-icon slot={"end"} name="add-circle-outline"> </ion-icon>
        </ion-item>
          </ion-list>
      </ion-card>

    ];
  }
}
