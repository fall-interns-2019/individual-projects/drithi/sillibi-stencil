import {Component, h, State} from '@stencil/core';

@Component({
  tag: 'app-courselist',
  styleUrl: 'app-courselist.css'
})
export class AppCourselist {
  @State() courses: any[];

  componentWillRender() {
      fetch("http://localhost:3000/api/v1/courses", {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json'
        },
      })
        .then(response => response.json())
        .then(json => this.courses= json);
    console.log(this.courses)
  }

  coursecard() {
    if(this.courses.length != 0) {
      return this.courses.map((course) => {
        return [
          <app-coursecard course_name = {course.course_name}
          course_number = {course.course_number}
          section_number = {course.section_number}
          term = {course.term}
          instructor = {course.instructor}
          id = {course.id}>
          </app-coursecard>
        ]
      })
    }
  }
  render() {
    return [
          <ion-header>
            <ion-toolbar color="purple">
              <ion-buttons slot="start">
                <ion-back-button defaultHref="/courses" />
              </ion-buttons>
              <ion-title>MY COURSES</ion-title>
            </ion-toolbar>
          </ion-header>,
      <ion-content>
        {
          this.coursecard()
        }
      <a href={"/addcourse"}> <img src={"../assets/icon/BTN_AddCourse.svg"} class="image"/> </a>
        </ion-content>
    ];
  }
}
