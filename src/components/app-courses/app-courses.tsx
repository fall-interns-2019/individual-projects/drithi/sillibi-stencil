import {Component, h,} from '@stencil/core';

@Component({
  tag: 'app-courses',
  styleUrl: 'app-courses.css'
})
export class AppCourses {

  render() {
    return [
      <ion-header>
        <ion-toolbar color="purple">
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/profile"/>
          </ion-buttons>
          <ion-title>MY COURSES</ion-title>
        </ion-toolbar>
      </ion-header>,

      <ion-content class={"ion-padding"}>
        <a href={"/addcourse"}> <img src={"../assets/icon/illo_AddCourse.svg"} class="center"/> </a>
        <p> <div text-center> Add a course to upload a syllabus </div> </p>
      </ion-content>

    ];
  }
}
