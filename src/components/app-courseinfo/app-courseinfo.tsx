import {Component, h, Prop, State} from '@stencil/core';

@Component({
  tag: 'app-courseinfo',
  styleUrl: 'app-courseinfo.css'
})
export class AppCourseInfo {
  @Prop() course_name:string;
  @Prop() course_number:string;
  @Prop() section_number:string;
  @Prop() term:string;
  @Prop() instructor:string;
  @Prop() id;
  @State() courseinfo: any;

  componentWillRender() {
    fetch(`http://localhost:3000/api/v1/courses/${this.id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
    })
      .then(response => response.json())
      .then(json => this.courseinfo= json);
    console.log(this.courseinfo)
  }

  courseinfocard() {
    return [
      <app-coursecard
        course_name = {this.courseinfo.course_name}
        course_number = {this.courseinfo.course_number}
        section_number = {this.courseinfo.section_number}
        term = {this.courseinfo.term}
        instructor = {this.courseinfo.instructor}
        id = {this.courseinfo.id}>
      </app-coursecard>
    ]
  }

render(){
  return [
    <ion-header>
      <ion-toolbar color="purple">
        <ion-buttons slot="start">
          <ion-back-button defaultHref="/courses"/>
        </ion-buttons>
        <ion-title>COURSE INFORMATION</ion-title>
      </ion-toolbar>
    </ion-header>,

    <ion-content class="ion-padding">
      {
        this.courseinfocard()
      }
    </ion-content>




    ];
  }
}
