import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-addcourse',
  styleUrl: 'app-addcourse.css'
})
export class AppAddCourse {
  // courseEndPoint = 'http://localhost:3000/api/v1/courses';
  course_name: HTMLIonInputElement;
  course_number: HTMLIonInputElement;
  section_number: HTMLIonInputElement;
  term: HTMLIonInputElement;
  instructor: HTMLIonInputElement;

  getCourseinput() {
    let courseinfo = {
      "course_name": this.course_name.value,
      "course_number": this.course_number.value,
      "section_number": this.section_number.value,
      "term": this.term.value,
      "instructor": this.instructor.value,
    };
    this.postCourseinfo(courseinfo)
  }
  postCourseinfo(courseinfo) {
    fetch("http://localhost:3000/api/v1/courses", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify(courseinfo)
    })
      .then(response => response.json())
      .then(json => console.log(json))
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar color="purple">
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/courses" />
          </ion-buttons>
          <ion-title>ADD A COURSE</ion-title>
        </ion-toolbar>
      </ion-header>,

      <ion-content class={"ion-padding"}>
        <ion-item>
          <ion-label position={"stacked"}> Course Name </ion-label>
          <ion-input ref={(el) => this.course_name = el as HTMLIonInputElement} required type="text" placeholder={"Business Administration"}> </ion-input>
        </ion-item>

        <ion-item>
          <ion-label position={"stacked"}> Course Number </ion-label>
          <ion-input ref={(el) => this.course_number = el as HTMLIonInputElement} required type="text" placeholder={"e.g., MGMT 100"}> </ion-input>
        </ion-item>

        <ion-item>
          <ion-label position={"stacked"}> Section Number </ion-label>
          <ion-input ref={(el) => this.section_number = el as HTMLIonInputElement} required type="text" placeholder={"e.g., 001"}> </ion-input>
        </ion-item>

        <ion-item>
          <ion-label position={"stacked"}> Term </ion-label>
          <ion-input ref={(el) => this.term = el as HTMLIonInputElement} required type="text" placeholder={"Fall 2017"}> </ion-input>
        </ion-item>

        <ion-item>
          <ion-label position={"stacked"}> Instructor </ion-label>
          <ion-input ref={(el) => this.instructor = el as HTMLIonInputElement} required type="text" placeholder={"Dr. Sam Birk"}> </ion-input>
        </ion-item>

        <ion-button href={"/courselist"} onClick={()=> this.getCourseinput()} expand={"full"} style={{marginTop: "30px", marginBottom: "40px"}} color={"yellow"}> ADD COURSE </ion-button>

      </ion-content>
    ];
  }
}
