import {Component, h,} from '@stencil/core';

@Component({
  tag: 'app-sign',
  styleUrl: 'app-sign.css'
})
export class AppSign {

  render() {
    return [
      <ion-header>
        <ion-toolbar color="purple">
          <ion-buttons slot="start">
            <ion-back-button defaultHref="/profile" />
          </ion-buttons>
          <ion-title>Sign In</ion-title>
        </ion-toolbar>
      </ion-header>,

      <ion-content class={"ion-padding"}>

        <ion-item>
          <ion-input required type="text" placeholder={"Email Address"}></ion-input>
        </ion-item>

        <ion-item>
          <ion-input required type="password" placeholder={"Password"}></ion-input>
        </ion-item>

        <ion-button href={"/courses"} expand={"full"} style={{marginTop: "30px", marginBottom: "20px"}} color={"yellow"}> LOGIN </ion-button>
        <ion-button href="https://facebook.com" expand={"full"} style={{marginBottom: "40px"}} color={"blue"}> LOGIN USING FACEBOOK </ion-button>

        <p> <div text-center> Don't have an account? </div> </p>
        <ion-button class="Register" href={"/profile"} fill={"clear"}>REGISTER</ion-button>

      </ion-content>

    ];
  }
}
